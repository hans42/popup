<?php
/**
 * Blocksy functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Blocksy
 */

if ( version_compare( PHP_VERSION, '5.7.0', '<' ) ) {
	require get_template_directory() . '/inc/php-fallback.php';
	return;
}

require get_template_directory() . '/inc/init.php';

    function load_css(){
        wp_register_style('index', get_template_directory_uri() . '/css/index.css', array(), false, 'all');
        wp_enqueue_style('index');	
        wp_enqueue_style('bootstrap',"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css");       
    }

    function my_script(){
        wp_enqueue_script('script', get_template_directory_uri() . '/popup_trigger/index.js', [], null, true);
    }
    
    function load_js()	
	{
		wp_register_script('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js', array('jquery'), '', true);
		wp_enqueue_script('bootstrap');
		
	}
    function custom_modal (){
    ?>

	<!-- Modal -->
	<div style="margin-top: 50px" class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<div class="modal-header">		
			<h3 class="modal-title" id="exampleModalLabel">Information Blog Hans Stoisser</h3>			
		</div>
		<div class="modal-body">
			<!-- Begin Mailchimp Signup Form -->
			<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">

			<!-- Begin Mailchimp Signup Form -->
	<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
	<style type="text/css">
		#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
		/* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
		We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
	</style>
	<div id="mc_embed_signup">
	<form action="https://hansstoisser.us1.list-manage.com/subscribe/post?u=967d95011330080615d5529fc&amp;id=52a9192195" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
		<div id="mc_embed_signup_scroll">
	<div class="mc-field-group">
		<label class="strong" for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
	</label>
		<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
	</div>
	<div class="mc-field-group">
		<label class="strong" for="mce-FNAME">First Name </label>
		<input type="text" value="" name="FNAME" class="" id="mce-FNAME">
	</div>
	<div class="mc-field-group">
		<label class="strong" for="mce-LNAME">Last Name </label>
		<input type="text" value="" name="LNAME" class="" id="mce-LNAME">
	</div>

	
	<h3 class="permissions">in English language </h3>
	<div class="checkbox">
		<input type="checkbox" value="" name="mce-English" class="" id="mce-English">
		<label for="mce-English">in English language </label>
	</div>
	<br />
	
	<h3 class="permissions">in deutscher Sprache </h3>
	<div class="checkbox">
		<input type="checkbox" value="" name="mce-Sprache" class="" id="mce-Sprache">
		<label for="mce-Sprache">in deutscher Sprache </label>
	</div>
	<br />

	<h3 class="permissions">Permissions </h3>
	<p class="text_sm">We will use the information you provide on this form to be in touch with you by the use of</p>

	<div class="checkbox">
		<input type="checkbox" value="" name="email" name="mce-email-permit" class="" id="mce-email-permit">
		<label for="mce-email-permit" class="text_sm">Email </label>
	</div>
	<br />

	<p class="text_sm">You can change your mind at any time by clicking the unsubscribe link in the footer of any email you receive from us.</p>
	<br />

	<div id="mce-responses" class="clear">
			<div class="response" id="mce-error-response" style="display:none"></div>
			<div class="response" id="mce-success-response" style="display:none"></div>
		</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
		<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_967d95011330080615d5529fc_52a9192195" tabindex="-1" value=""></div>
		<div class="clear" style="display: flex; align-items: center; justify-content: space-between;">
			<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
			<button type="button" class="close-btn" data-dismiss="modal">Close</button>
		</div>
		</div>
	</form>
	</div>
	<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';fnames[5]='BIRTHDAY';ftypes[5]='birthday';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
	<!--End mc_embed_signup-->
	
	<?php
		}
		add_action('wp_footer', 'custom_modal' );
		add_action('wp_enqueue_scripts', 'load_css');
		add_action('wp_enqueue_scripts', 'my_script');
		add_action('wp_enqueue_scripts', 'load_js');