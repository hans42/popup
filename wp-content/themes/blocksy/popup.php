<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .wrapper{
            background: rgba(0,0,0,0.7);
            width: 100%;
            height: 100vh;
            display: none;
            justify-content: center;
            align-items: center;
        }

        .form{
            background: white;
            width: 50%;
            height: 400px;
        }

        .wrapper.open{
            display: flex;
        }
    </style>
</head>
<body>

    <button id="open_btn">Open model</button>

    <div class="wrapper" id="model">
        <div class="form">
        <button id="close_btn">Close model</button>
        </div>
    </div>

    <script>
        const open = document.querySelector('#open_btn');
        const close = document.querySelector('#close_btn');
        const model = document.querySelector('#model');

        open && open.addEventListener('click', () => model.classList.add('open'));
        close && close.addEventListener('click', () => model.classList.remove('open'));
    </script>
    
</body>
</html>

<?php
/* 
    Template Name: Popup Page
*/

?>