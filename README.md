# POP-UP TASK

## The following are steps for adding a responsive popup on Hans website:


- **Step 1**
 
    Open `function.php` file which is located at `wp-content/themes/blocksy`
               
     A. This file contains: 
        -  popup HTML code, 
        -  scripts for loading bootstarp CSS and Jquery 
     
     B. Therefore, copy all code in this file and paste them in respective `Han's website function.php file`
   3. Save those changes


- **Step 2**

     A. In Create a new folder called `popup_trigger` with `index.js` file in `wp-content/themes/blocksy`  directory in **Hans website codebae**

     B. Copy all code from `popup_trigger's` index file and past them in the created same folder on the above step(A)

     C. Code in the above  `index.js` file contains the logic of adding `attributes` on both newsletter desktop and mobile buttons, that bootstrap uses in order to add event listeners which triggers the popup. On side not, the file is used in `function.php` 

