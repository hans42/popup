<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'hans' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'L~4p2$>c~@(D:r)#j#|K-IPsliSY*|ZwYa#8gC_<!,O0DoLvQ,Kp3Fc!~L%_[J5z' );
define( 'SECURE_AUTH_KEY',  'GL+u{Pi@8MLaYdTD>ofNI.2Ol8~Gz/KKs|&_Zh[$.Bi)oh*]^h-SX|G_(jqi<Z_J' );
define( 'LOGGED_IN_KEY',    '_5~dC?FDqsT4o03JI}}Be.!&s@e]i%JZKYYf5>0k]@s`q}eT>5^AhqOtH_&C@#h1' );
define( 'NONCE_KEY',        '}I{_ND0RwekXBTIq7:zJv-sph>_5~<=oiu@<pybDfD:=Uqrk? }pIKX/l%01%Or{' );
define( 'AUTH_SALT',        'SV1o}<]P*8}qpDBb&0la2mQ|Bz~enRossy-00XgU11KVW7RfeTwjG).XB##-n-Uu' );
define( 'SECURE_AUTH_SALT', 'd_/m&WpVF B+kV AX7iV80qjyexxRfdpkm#)<nc(8>je:,!p2b)=p((!4VgwA>pQ' );
define( 'LOGGED_IN_SALT',   '|2V3?wUDuxjXmX]l$6`@KpCX+[w9.c>L0|^:RSXsxh>{gQ#~&6?N23>Rj^&=,kg2' );
define( 'NONCE_SALT',       'UWI%:`UqYRIE%)+,;v q]]tW3;7c]u?N.j$jbIcwn]Nc<-{NM!j+4+map2&&DgA}' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
